---
title: "Examples"
draft: false
weight: 15
chapter: false
---


## Request

```json
{
  "header": {
    "action": "iotrabbitmq.api.getpos",
    "etag": "a41d6cc6d92b52a9ae8f127b9f335bdac8af9dcebaf847c02ba489ff",
    "code": "000",
    "document_datetime_init": 123423424324
  },
  "body": {
    "device_id":213,
  },
}
```

## Response Example

``` json
{
  "header": {
    "action": "iotrabbitmq.api.pos",
    "etag": "a41d6cc6d92b52a9ae8f127b9f335bdac8af9dcebaf847c02ba489ff",
    "code": "000",
    "document_datetime_init": 123423424452
  },
  "body": {
    "device_id":213,
    "gps":{
        "latitude": 41.2324123,
        "altitude": 23.23423434,
        "speed": 120
    },
    "device": {
        "battery": 12,
        "snr": 0
    }},
"api_timespan": 2123
}}
```
