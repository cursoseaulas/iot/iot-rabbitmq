---
title: "Global Document"
draft: false
weight: 5
chapter: false
---

## Key Table

| Key | Value | Format|
| --- | ----------- |------| 
| header.action |  iotrabbitmq.api.$ACTION | String |
| header.etag | Request’s etag | String |
| header.code | Request’s code | String |
| header.b'timespan | Request’s timespan | String |
| body.gps.latitude | GPS Latitude| float |
| body.gps.longitude | GPS Longitude| float |
| body.gps.altitude | GPS Altitude | Integer |
| body.gps.speed | GPS Speed | Integer |
| body.gps.battery | GPS Battery | float |
| body.device_id | Device IDentifier | float |

## Request

```json
{
  "header": {
    "action": "iot-rabbitmq.api.$ACTION",
    "etag": "$ETAG",
    "code": "$CODE",
    "document_datetime_init": "$DATETIME_INIT"
  },
  "body": {
      "device_id":"$DEVICE_ID",
      "gps":{
          "latitude": "$LATITUDE",
          "altitude": "$ALTITUDE",
          "speed": "$SPEED",
      },
      "device": {
              "battery": "$BATTERY"
      },
  "api_timespan":"$API_TIMESPAN"
}}
```
## Error Code

| Status | Description |
| --- | ----------- |
| IOT-ERROR-001 | not a valid action |

# Ticket Value

## Request

```json
{
  "header": {
    "action": "iot-rabbitmq.api.ticket_value",
    "etag": "$ETAG",
    "code": "000",
    "document_datetime_init": "$DATETIME_INIT"
  },
  "body": {},
}
```

## Response

```json
{
  "header": {
    "action": "iot-rabbitmq.api.ticket_value",
    "etag": "$ETAG",
    "code": "000",
    "document_datetime_init": "$DATETIME_INIT"
  },
  "body": {
        "Normal": 4.2,
        "Student": 2.2,
        "Elderly": 0
  },
  "api_timespan":"$API_TIMESPAN"
}
```


# Next Bus Station

## Request

## Response




# Errors Code

| Status | Description |
| --- | ----------- |
| IOT-ERROR-001 | not a valid action |
