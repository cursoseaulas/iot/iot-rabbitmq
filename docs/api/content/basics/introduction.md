---
title: "Introdution"
draft: false
weight: 5
chapter: false
---

In this manual you will learn how to connect and use the GUI API.

The IoT-RabbitMQ engine provide a device interaction.

The IoT-RabbitMQ API communication scheme can be seen from figure:

WIP

<!-- #### Stage 1: Patient Monitoring

{{<mermaid>}}
sequenceDiagram

    participant QuoreOne
    participant QuoreApp
    participant UTOS
    participant QuoreAI
    participant Database

    loop Record_ECG
    	QuoreOne->> QuoreOne: Record package (5s)
    end
	QuoreOne->> QuoreApp: QuoreOne.package.tx
    Note left of QuoreApp: Bluetooth Communication
	loop Storage_ECG
		QuoreApp ->> QuoreApp: Store ECG package (.bin)
	end
    QuoreApp->> UTOS: QuoreApp.package.tx
    Note right of QuoreApp: TCP Communication
	UTOS->>QuoreAI: QuoreAI.evaluate
	loop Physician ML Model
        QuoreAI->>QuoreAI:  QuoreAI.evaluate.annotation
    end
    QuoreAI->> UTOS: QuoreAI.annotation
    UTOS ->> Database: QuoreAI.annotation
 {{< /mermaid >}}


#### Stage 2: Data Visualization

{{<mermaid>}}
sequenceDiagram

    participant QuoreApp
    participant UTOS
    participant QuoreAI
    participant Database
    participant FrontEnd

    QuoreApp->> UTOS: tcp.finish.monitoration
	UTOS->>Database: get.ecg.annotation
	Database->>UTOS: ecg.annotation
	UTOS->>QuoreAI: QuoreAI.qrsgrouping
	loop QRS_GROUP
        QuoreAI->>QuoreAI: Clustering QRS Frames
    end
    QuoreAI->> UTOS: QuoreAI.qrsgrouping.qrsclusters
    UTOS ->> Database: QuoreAI.qrsgrouping.qrsclusters
    FrontEnd ->> UTOS: frontend.request.ecgdata
    UTOS ->> Database: UTOS.request.ecgdata
    UTOS ->> FrontEnd: UTOS.produce.ecgdata
 {{< /mermaid >}}

#### Stage 3: Reclassification

{{<mermaid>}}
sequenceDiagram

    participant Doctor
    participant FrontEnd
    participant UTOS
    participant Database

    Doctor->>FrontEnd: doctor.change.classification
    FrontEnd->> UTOS: doctor.change.classification
    UTOS ->> Database: change.ecg.annotation
    FrontEnd ->> UTOS: frontend.refresh.page
    UTOS ->> Database: get.annotation
    
  
 {{< /mermaid >}}

#### QuoreAI: Description

{{<mermaid>}}
sequenceDiagram
    participant UTOS
    participant QuoreAI
    participant TF_Serving
    UTOS ->> QuoreAI: uuid_physician
    UTOS ->> QuoreAI: ecg raw data [lead II]
    UTOS ->> QuoreAI: exam parameters
loop apply qrs frames
        QuoreAI->>QuoreAI: Baseline Filter 
        QuoreAI->>QuoreAI: Map QRS Frames  
    end

    QuoreAI ->> TF_Serving: uuid_physician
    Note right of TF_Serving: load Physician<br/> Tensoflow models
    QuoreAI ->> TF_Serving: QRS Frames
    TF_Serving ->>   QuoreAI: Morphology Annotation
    QuoreAI ->> UTOS: uuid_physician
    QuoreAI ->> UTOS: Morphology Annotation
    {{< /mermaid >}} -->